package com.gitlab.sokoban.infra;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/")
public class FrontDispatcher {

    public FrontDispatcher() {
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    String move(@RequestParam(value = "move", required = false) String move, Model model) {

        model.addAttribute("squares", Arrays.asList(
                Arrays.asList("wall", "wall" , "wall", "wall"),
                Arrays.asList("wall", "empty" , "empty", "wall"),
                Arrays.asList("wall", "empty" , "empty", "wall"),
                Arrays.asList("wall", "wall" , "wall", "wall")

        ));
        return "index.html";
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    String index(Model model) {

        model.addAttribute("squares", Arrays.asList(
                Arrays.asList("wall", "wall" , "wall", "wall"),
                Arrays.asList("wall", "empty" , "empty", "wall"),
                Arrays.asList("wall", "empty" , "empty", "wall"),
                Arrays.asList("wall", "wall" , "wall", "wall")

        ));
        return "index.html";
    }
}


