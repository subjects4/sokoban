# Sokoban

The [Sokoban](https://en.wikipedia.org/wiki/Sokoban) is a puzzle game in which the player has to move all boxes to their storage locations

## Rules

The player can move horizontally and vertically, but never through a wall or a box.

They can only push boxes. And, the boxes can not be moved on a square with another box or a wall.

The number of boxes equals the number of storage locations.

The puzzle is solved when all boxes are placed at storage locations.

A map is defined by a rectangle (W by H) of squares.

Each square has a state :
- Wall: nothing can be moved on this squares
- Floor: a free squares on which the player or a box can be moved
- Player: the current player position
- Box: a box to move to a storage
- Storage: a floor square marked as destination for a box
- Stored Box: a box on a storage location



## 1. Map

Every class of this part must be implemented in the package `com.gitlab.sokoban.domain.model`.

### 1.1. Positions and squares

A position is defined by two integers X and Y. These coordinates cannot be changed once created

A square is defined by a position and a state:
- Player
- Wall
- Empty
- Box
- Storage
- BoxStored

A square cannot be changed after its creation

# 1. Create the enum `State`
# 2. Create the class Position and Square
# 3. Two positions should be equal if the Xs and the Ys are equal. Create a test to check this property
# 4. Two squares should be equal if the position and the state are equal. Create a test to check this property

### 1.2. Map

A map contains a list of squares.

It can say if the square at a given position is a wall or not

It can say if a position is in the map or not. For example, on the map of 4x4, the position 12;34 is not inside the map. But the position 2;2 is inside the map.

# 1. Create the class `Map`. A map is created from a size and a list of squares
# 2. Create the method `isWall`. It returns true if the given position is a wall. False otherwise
# 3. Create the method `inside`. It returns true if the given position is in its boundaries. False otherwise
# 4. Two maps are equal if their sizes and all their squares are equal.

### 1.2. Generator

A map is generated from a text file containing the state of each square, as below:
```
    #####
    #   #
    #$  #
  ###  $##
  #  $ $ #
### # ## #   ######
#   # ## #####  ..#
# $  $          ..#
##### ### #@##  ..#
    #     #########
    #######
```

`#` : wall
`$` : box
`.` : storage
`@` : player
` `: floor

For now, we are building the map. We will only focus on the walls and floors.


1. Create a class `Utils` which will be used to convert a string to a list of squares
2. Create the static method `toTiles`. It takes a `String` as an input and returns a list of `Square`
3. Create a class `MapBuilder` with a static method `build`. It takes a `String` as input and returns a `Map`


## 2. The game

Once we have our map, we can try to display it


### 2.1. Sokoban

A game is represented by an object of the class `Sokoban` which will contain the map, the player position, the boxes, etc.

1. Create a class ``